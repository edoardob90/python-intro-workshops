import string
from en_dict import words

def is_prime(n):
    """Check if N is prime"""
    if n == 1:
        return False
    elif n == 2:
        return True
    for i in range(2, n):
        if n % i == 0:
            break
    else:
        return True
    return False

def primes(n):
    """Generate the first N primes"""
    num = 2
    n_primes = []
    while len(n_primes) < n:
        if is_prime(num):
            n_primes.append(num)
        num += 1
    return n_primes

# primes for each letter in the range a-z
letters_primes = primes(len(string.ascii_lowercase))

# mapping between a-z and primes
mapping = dict(zip(string.ascii_lowercase, letters_primes))

# if the product of primes of the two words is equal, words are anagrams
def are_anagrams(word_1, word_2, *, verbose=False):

    numeric_words = []

    for word in [w.lower() for w in word_1, word_2]:
        numeric_word = 1
        for letter in word:
            if letter in mapping:
                numeric_word *= int(mapping[letter])
        numeric_words.append(numeric_word)

        if verbose:
            print(f"Word '{word}' corresponds to {numeric_word}")

    return not(bool(numeric_words[1] - numeric_words[0]))

def run():
    title = 'Checking anagrams with prime numbers!'
    print(title)
    print('='*len(title))

    word_1 = input("Insert the first word: ")
    word_2 = input("Insert the second word: ")

    if not word_1:
        raise RuntimeError("First word cannot be an empty string.")

    if word_2:
        if are_anagrams(word_1, word_2, verbose=True):
            print(f"'{word_1}' is an anagram of '{word_2}'")
        else:
            print(f"Words '{word_1}' and  '{word_2}' aren't anagrams")
    else:
        anagrams = []
        for word in words:
            if are_anagrams(word_1, word):
                anagrams.append(word)
        anagrams.remove(word_1.lower())
        print(f"Found {len(anagrams)} {'anagram' if len(anagrams) == 1 else 'anagrams'} of '{word_1.lower()}':\n{anagrams}")


if __name__ == "__main__":
    run()
