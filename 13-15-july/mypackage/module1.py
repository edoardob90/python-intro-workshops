# module1.py

def greet(name):
    """Greet the user by printing his/her name"""
    print(f"Hello, {name}!")