# Access and run the Jupyter notebooks

All the workshop has been carried out in Jupyter notebooks. If you don't know what they are, for now it's enough to say that they provide an **interactive Python environment** with many more features than a simple Python console. You can go to [Jupyter.org](https://jupyter.org/) and learn more!

If you want to execute any of the notebooks we went through during the workshop, you can access them on [Binder.org](https://mybinder.org/v2/gl/ebaldi%2Fpython-intro-workshops/HEAD?urlpath=lab).

# Zoom recordings

- [Day 1](https://us02web.zoom.us/rec/share/fGlhKZQVAKFoeicVSZoLQzo34QAQQBKkHIB2g8Ig9frL7VWeD5QpnQMkN8cVLnz8.xRrtGs-gXIgoc14Y) (Passcode: `7iF4*9Gm`)

- [Day 2](https://us02web.zoom.us/rec/share/jCLXGo0oUikcbehRADGddKTyKQphcOUSvAyDBjpYZv9oxnoDG0WX6MMyvO9aYGZG.i6rMHf5ZerSeDOGW) (Passcode: `nSA7T=sU`)

- [Day 3](https://us02web.zoom.us/rec/share/5Jj5Jomyc5AK4pUn4gnFqNuk8DtsxPVwmkHncYlYBRI_-VfsQmyZzuyFThssuDpV.nL5vT_XrnnSe4fie) (Passcode: `H$3HN1PL`)
