# Import the random module
import random

# Tuples
my_tuple = ("Edoardo", 1, 2, 3, 20.0, (1, 2, 3))
my_tuple[2]

my_second_tuple = tuple("Python")

# Get the length of a tuple
#print(len(my_tuple))

# List
my_list = [1., 2., 3.]  # list literals
my_second_list = list("Python")
#print(my_second_list)

my_second_list[0] = "Z"
#print(my_second_list)

colors = ["Orange", "Blue", "Magenta", "Brown"]

# split()
names = """Eric,David,Renzo,Lucia"""
names_list = names.split(',')

# Reassign
#print(names_list)
#names_list[1:3] = ["George", "Luke", "Obi-Wan"]
#print(names_list)

# Methods to adding and removing elements
# insert()
names_list.insert(10, "Leia")
#print(names_list)

# pop()
elem = names_list.pop(0)
#print(elem)
#print(names_list)

# append()
#your_name = input("Enter your name: ")
#your_friend_name = input("Enter your friend's name: ")
#names_list.append(your_name)
#print(names_list)

# extend()
#bad_guys = ["Darth Vader", "Palpatine"]
#names_list.extend(bad_guys) # my_tuple + (1, 2, 3)
#print(names_list)

# Lists of numbers
my_range = range(25)
ints = list(my_range)
#print(ints)

#print(
#    f"The smallest is {min(ints)}\nThe largest is {max(ints)}\nThe sum is {sum(ints)}"
#)

# List comprehension
other_ints = ints[:10]

# Square the elements and add a random int
#squares = []
#for element in other_ints:
#  squares.append(element ** 2 + random.randint(-10, 10))

squares = [element**2 + random.randint(-10, 10) for element in other_ints]

str_numbers = ["1.3", "2e6", "1_000.4637"]
float_numbers = [float(element) for element in str_numbers]
#print(float_numbers)

# Nesting
matrix_1 = [[1, 2], [3, 4]]
#[ 1   2 ]
#[ 3   4 ]

# Copying
matrix_2 = matrix_1[:]  # CAREFUL: this is shallow copying matrix_1
matrix_2[0] = [5, 6]

matrix_2[1][0] = "1"

# matrix_2 = matrix_1.deepcopy()

# Sorting
# .sort()
ints = [3, 4, 1, 9]
ints.sort()

items = [(3, 8), (4, 1), (1, -5), (9, 2)]


def by_second(item):
    return abs(item[1])


items.sort(key=by_second)

# Exercise: make poetry
#import poetry

#my_poem = poetry.make_poem()

#print(my_poem)

# Dictionaries

# dictionary literals
capitals = {
    "CA": "Sacramento",
    "TX": "Austin",
    "NY": "Albany",
    "AZ": "Phoenix"
}

# built-in dict()
state_cities = (("CA", "Sacramento"), ("NY", "Albany"), ("AZ", "Phoenix"),
                ("TX", "Austin"))

capitals = dict(state_cities)

# Add or remove
capitals["FL"] = "Miami"
del capitals["TX"]

# Membership
#"key" in capitals

# Iterating over dicts

    # Keys are USUALLY string
    # Valid keys are
    # - strings
    # - integers
    # - float
    # - booleans
    # - tuples
temperature_map = {
  (0, 0, 0): 25.6, 
  (1, 2, 3): 24.9
  }

# Nesting dictionaries
cities_temperature = {
    "CA": {
      "Capital": "Sacramento",
      "Temperature": {
        "January": 12.0,
        "March": 21.0,
        "July": 32.4
      }
    },
    "TX": "Austin",
    "NY": "Albany",
    "AZ": "Phoenix"
}

#print(cities_temperature["CA"]["Capital"])
#print(cities_temperature["CA"]["Temperature"])

# Dictionary comprehension
friends = ["Pablo", "Nora", "Jen", "Ian"]
age = [35, 36, 35, 38]
times = [3, 7, 15]

def time_seen(index):
  try:
    return times[index]
  except IndexError:
    return 0

friends_dict = {
  friends[i]: time_seen(i)
  for i in range(len(friends))
}

def expand_list(full_list, short_list):
  return [x if x in short_list else 0 for x in full_list]

short_list = [1, 2, 3]
full_list = list(range(15))

# Sets
# Mutable and unordered collections of immutable objects
art_friends = {"Pablo", "Nora", "Jen", "Ian"}
sci_friends = {"Jen", "Charlie"}

# empty_set = {}

# .add()
art_friends.add("Luke")

# .remove()
art_friends.remove("Luke")

# .discard()
art_friends.discard("Leia")

# Compare sets
# Complement: elements in one set but not in the other
art_no_sci = art_friends.difference(sci_friends)
print(art_no_sci)

# Symmetric difference: elements which AREN'T in both sets
sym_diff = art_friends.symmetric_difference(sci_friends)
print(sym_diff)

# Intersection
print(art_friends.intersection(sci_friends))
art_friends & sci_friends

# Union
art_friends.union(sci_friends)
print(art_friends | sci_friends)

# Sets comprehension
my_friends = ["Pablo", "nora", "JEN", "Ian"] # my friends

friends_lower = {name.lower() for name in my_friends}
print(friends_lower)

guest = ["Jen", "Charlie", "PABLO", "luke"]
guest_lower = {name.lower() for name in guest}

present_friends = {name.capitalize() for name in friends_lower & guest_lower}
print(present_friends)

# Last things about comprehensions: 'zip' and 'enumerate'

friends = ["Rolf", "Bob", "Jen", "Anne"]
time_since_seen = [3, 7, 15, 11]

# Dictionary comprehension
long_timers = {
    friends[i]: time_since_seen[i]
    for i in range(len(friends))
    if time_since_seen[i] > 5 # and if-clause in a dictionary comprehension
}

# Can we make it even more compact and "Pythonic"? Yes, with 'zip'
# zip "packs" multiple iterables' elements in a sequence of tuples
long_timers = dict(zip(friends, time_since_seen))
print(long_timers)

# enumerate
counter = 0
for friend in friends:
  #print(counter, friend)
  counter = counter + 1

for counter, friend in enumerate(friends, start = 1):
  print(counter, friend)