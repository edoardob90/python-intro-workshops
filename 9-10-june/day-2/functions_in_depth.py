def yell(text):
    return text.upper() + "!"


# 1. We can assign a name to a function
bark = yell
#print(bark('woof'))

# unique string for each function
#print(bark.__name__)  # dunder variables

# 2. Store functions in data structure
funcs = [bark, yell, str.lower, str.capitalize]

#print(funcs)
#print(funcs[0]('woof'))


# 3. Functions can be passed to other functions
def greet(func):
    """This is a HIGHER-LEVEL function"""
    greeting = func("Hi, I'm a Python program!")
    print(greeting)


#greet(bark)

# Example: the built-in function 'map'
#print(list(map(bark, ['hi', 'hey', 'ahoy'])))


# 4. Functions can be nested
def speak(text):
    def whisper(t):
        return t.lower() + '...'

    return whisper(text)


#print(speak('HELLO'))


def speak_func(volume):
    def whisper(t):
        return t.lower() + '...'

    def yell(t):
        return t.upper() + '!'

    if volume > 0.5:
        return yell
    else:
        return whisper


yell = speak_func(0.7)


# 5. Functions can capture the local state
def speak_func(volume, text):
    def whisper():
        """This function is a CLOSURE"""
        return text.lower() + '...'

    def yell():
        return text.upper() + '!'

    if volume > 0.5:
        return yell
    else:
        return whisper


#print(speak_func(0.7, 'hello')())


# Example
def make_adder(n):
    def add(x):
        return x + n

    return add


plus_3 = make_adder(3)
plus_5 = make_adder(5)


def func(required, *args, **kwargs):
    print(required)
    if args:
        print(args)
    if kwargs:
        print(kwargs)


#func('hello')
#func('hello', 'world')
#func('hello', 'world', 'WORLD', status = 'very good')


def bar(x, y, nickname, **kwargs):
    print("Required: ", x, y)
    if 'surname' in kwargs:
        full_name = kwargs['name'] + ' ' + kwargs['surname']
    else:
        full_name = nickname
    print("Keyword: name = ", full_name)


def foo(x, *args, **kwargs):
    kwargs['name'] = 'Alice'
    new_args = args + ('extra', )
    print(*new_args)
    bar(x, *new_args, **kwargs) # * is unpacking the positional arguments and is passing them as a sequence

#foo(50)
foo(50, 100, "Ali")

# * and ** unpacking operators
def print_vector(x, y, z):
  print(f"[{x}, {y}, {x}]")

vec = (1.0, -9.0, 3.0)
#print_vector(vec[0], vec[1], vec[2]) # ugly and verbose
#print_vector(*vec)

dict_vec = {
  'x': 4.0,
  'y': 5.0,
  'z': -10.0
}

#print_vector(**dict_vec)


#<namespace>.<name>

# 1.
# import random # just the namespace

# 2.
# from random import randint, choice # list of names from the namespace

# 3.
#import random as rd

# 4.
#from random import choice as rc