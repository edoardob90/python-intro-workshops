"""
Write a program tha converts between Celsius and Fahrenheit

The program should have 2 functions:

  - convert_cel_to_far(): one float input representing a degrees Celsius temperature, returns one float representing the converted temperature

  - convert_far_to_cel(): one float input, one float output. Does the opposite

Recall the conversion formula: F = C * 9/5 + 32

The program should

  1. First, prompt the user to enter a temperature in degrees Fahrenheit and then display the temperature converted to Celsius.
  2. Then prompt the user to enter a temperature in degrees Celsius and display the temperature converted to Fahrenheit.
  3. Display all converted temperatures rounded to 2 decimal places.

"""