"""
Write a program that simulates an election according to the following rules:

- Suppose two candidates, Candidate A and Candidate B, are running for mayor in a city with three voting regions. The most recent polls show that Candidate A has the following chances for winning in each region:

  - Region 1: 87% chance of winning
  - Region 2: 65% chance of winning
  - Region 3: 17% chance of winning

- Write a program that simulates the election 10,000 times and prints the percentage of where Candidate A wins.

- To keep things simple, assume that a candidate wins the election if they win in at least two of the three regions.
"""