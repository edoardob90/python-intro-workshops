import random

num_trials = 10_000

times_A_wins = 0
times_B_wins = 0

for trial in range(num_trials + 1):
    #print(f"This is trial {trial} out of {num_trials}")
    votes_A = 0
    votes_B = 0

    # 1st region
    if random.random() < 0.87:
        votes_A += 1
    else:
        votes_B += 1

    # 2nd region
    if random.random() < 0.65:
        votes_A += 1
    else:
        votes_B += 1

    # 3rd region
    if random.random() < 0.17:
        votes_A += 1
    else:
        votes_B += 1

    # Determine which candidate wins the election
    if votes_A > votes_B:
        times_A_wins += 1
    else:
        times_B_wins += 1

print(f"Probability of A winning : {times_A_wins / num_trials}")
print(f"Probability of B winning : {times_B_wins / num_trials}")