# A more Pythonic way
def translate_better(input_string):
    old = "baelost"
    new = "4831057"
    # Build a translation table
    # arguments MUST BE strings
    translation_table = str.maketrans(old, new)
    # Return the string translated according to the table
    return input_string.translate(translation_table)

sentence = input("Please, enter the string to translate: ")

print(translate_better(sentence))