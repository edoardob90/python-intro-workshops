# Find the sum

1. You have a list of integers (check out `ints.txt`). You need to find **the pair of numbers** whose sum is 2020 and calculate their product.

2. Can you calculate the product of **three numbers** which sum to 2020?

---

*If you have already some experience with Python, you can give this exercise a try. You must already know something about loops, lists, and how to read from an input file. If you don't know or remember how to do any of these, we will work out the solution together.*

