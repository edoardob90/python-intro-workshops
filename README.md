# Python Intro Workshops

Collection of materials related to the Python Introductory Workshops. Such materials include:

1. Live coding sources/notebooks (almost all the code that has been discussed during the workshop)

2. Exercises and solutions

3. [Additional links](references.md) to delve more into some of the topics

Note that, while the overall program the workshops is the same, some topics might have been skipped or discussed more in depth on different dates. At first, look at the material for the ate you attended.

#### Virtual class on Replit.com

The virtual classes on Repl.it **are no longer available**. All the material is now in this repository.

~~The virtual class **is still accessible** at  for as long as the workshops are scheduled.~~

- ~~[Here](https://replit.com/team/empa01) if you attended the workshop in **June**.~~
- ~~[Here](https://replit.com/team/pyempa) if you attended the **July** workshop~~

#### How to run Jupyter notebooks

You can re-run the interactive Jupyter notebooks online by following the instructions in the corresponding README file:

- Participants to the **16-17-18 June** workshop, go [here](16-18-june/README.md)

- Participants to the **13-14-15 July** workshop, go [here](13-15-july/README.md) 

Of course, participants to the other dates can do that as well!
